# Slicing psd
==============

## Getting Started

## Requirements
You'll need [Node.js](https://nodejs.org), [Grunt](http://gruntjs.com/using-the-cli) and [Bower](http://bower.io/).

## Setup
```
git clone git@bitbucket.org:Cyberdyne/hackconf2016.git hackconf2016
cd hackconf2016
npm install --save-dev && bower update --save
grunt build
```

