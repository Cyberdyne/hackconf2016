module.exports = function(grunt) {
	require('load-grunt-tasks')(grunt);
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		fixturesPath: "src",

		clean: {
			build: {
				src: ['dist/']
			}
		},

	    sass: {
		options:  {
		sourceMap: true
		},
		dist: {
		files:  {
                  'dist/css/main.css' : 'scss/app.scss'
                }
		}
		},

		csslint: {
			build: {
				options: {
					csslintrc: 'scss/.csslintrc'
				},
				src: [
					'dist/css/main.css'
				]
			}
		},
		copy: {
			main: {
				files: [
					{
						expand: true,
						src: 'fonts/*',
						dest: 'dist/',
						filter: 'isFile'
					},
					{
						expand: true,
						src: 'images/*',
						dest: 'dist/',
						filter: 'isFile'
					}
				]
			}
		},
		autoprefixer: {
			options: {
				cascade: true,
				browsers: ['last 2 versions', 'ie >= 9', 'and_chr >= 2.3']
			},
			build: {
				files: {
					'dist/css/main.css': ['dist/css/main.css']
				}
			}
		},

		csso: {
			options: {
				report: 'min'
			},
			build: {
				files: {
					'dist/css/main.min.css': ['dist/css/main.css']
				}
			}
		},


		// concat: {
		// 	build: {
		// 		src: [
		// 			'js/scripts.js'
		// 		],
		// 		dest: 'dist/js/scripts.js',
		// 		nonull: true
		// 	}
		// },

		htmlbuild: {
			dist: {
				src: './src/templates/*.html',
				dest: './dist/',
				options: {
					beautify: true,
					//recursive: true,
					relative: true,
					styles: {
						bundle: [
							'<%= fixturesPath %>/css/main.css'
						]
					},
					sections: {
						templates: '<%= fixturesPath %>/templates/*.html',
						part: {
							header: '<%= fixturesPath %>/partitions/header.html',
							footer: '<%= fixturesPath %>/partitions/footer.html'
						}
					}
				}
			}
		},

		watch: {
			css: {
				files: ['scss/**/*.scss'],
				tasks: ['sass', 'autoprefixer']	,
				options: {
					spanw: false
				}

			},
			js: {
				files: ['js/**/*.js'],
			 	tasks: ['babel:dist'],
				options: {
					spawn: false
				}
			},
			html: {
				files: ['src/**/*.html'],
				tasks: ['htmlbuild'],
				options: {
					spawn: false
				}
			}
		},

		browserSync: {
			dev: {
				bsFiles: {
					src : [
						'dist/css/*.css',
						'dist/*.html'
					]
				},
				options: {
					watchTask: true,
					proxy: "http://localhost:8080/"
				}
			}
		},

		babel: {
			options: {
				sourceMap: true,
				presets: ['es2015']
			},
			dist: {
				files: [{
					expand: true,
					src: ['js/**/*.js'],
					dest: 'dist/'
				}]
			}
		}
	});

	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-csslint');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-csso');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-html-build');
	grunt.loadNpmTasks('grunt-browser-sync');
	//grunt.loadNpmTasks('grunt-contrib-concat');

	grunt.registerTask('default', ['clean', 'sass', 'copy', 'htmlbuild', 'babel', 'browserSync', 'watch']);
	grunt.registerTask('build', ['clean', 'sass', 'copy', 'autoprefixer', 'htmlbuild', 'babel']);
};
